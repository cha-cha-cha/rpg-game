using UnityEngine;

public class BookCollect : MonoBehaviour
{
    public GameObject spellsUI;
    public GameObject magicUI;
    private bool magicCollected = false;
    private bool spellsCollected = false;
    public bool magicBook = false;
    public bool spellsBook = false;

    // Start is called before the first frame update
    void Start()
    {
        /*spellsUI.SetActive(false);
        magicUI.SetActive(false);*/
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (magicBook)
            {
                if (!magicCollected)
                {
                    magicUI.SetActive(true);
                    magicCollected = true;
                }
            }
            if (spellsBook)
            {
                if (!spellsCollected)
                {
                    spellsUI.SetActive(true);
                    spellsCollected = true;
                }
            }
        }
    }
}
