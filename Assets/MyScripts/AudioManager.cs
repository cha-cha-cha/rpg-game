using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private AudioSource audioPlayer;
    public AudioClip mainLoop;
    public AudioClip tavernLoop;
    public AudioClip battleLoop;
    public int musicState = 1;
    [HideInInspector]
    public bool canPlay = true;

    void Start()
    {
        audioPlayer = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if (canPlay)
        {
            canPlay = false;
            switch (musicState)
            {
                case 1:
                    audioPlayer.clip = mainLoop;
                    audioPlayer.Play();
                    break;
                case 2:
                    audioPlayer.clip = tavernLoop;
                    audioPlayer.Play();
                    break;
                case 3:
                    audioPlayer.clip = battleLoop;
                    audioPlayer.Play();
                    break;
            }

        }
    }
}
