using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class MessageScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Text buttonText;
    public Text shopOwnerMessage;
    public Color32 messageOff;
    public Color32 messageOn;
    public GameObject[] shopUI;
    [HideInInspector]
    public int shopNum = 0;

    public void OnPointerEnter(PointerEventData eventData)
    {
        buttonText.color = messageOn;
        PlayerMovement.canMove = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        buttonText.color = messageOff;
        PlayerMovement.canMove = true;

    }

    private void Start()
    {
        shopOwnerMessage.text = "hello " + SaveScript.pname + " how can i help you";
    }

    private void Update()
    {
        if (PlayerMovement.canMove && PlayerMovement.isMoving)
        {
            if (shopUI != null)
            {
                shopUI[shopNum].SetActive(false);
            }
        }


    }

    public void Message1()
    {
        shopOwnerMessage.text = "not much going on around here";
    }

    public void Message2()
    {
        shopOwnerMessage.text = "select items from the list";
        shopUI[shopNum].SetActive(true);
        shopUI[shopNum].GetComponent<BuyScript>().UpdateGold();
    }
}
